# pycascrel

## About

This is a python bindings project wrapping around the cascrel library.

It uses Conan to fetch all dependencies.

## Installation guide

Run in project root:

`pip install .`

or, if you prefer a local user-site installation:

`pip install --user .`
