.. pycascrel documentation master file, created by
   sphinx-quickstart on Sun Sep 29 14:34:29 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pycascrel's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 3

   pycascrel-api-reference
